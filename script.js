$(function(){

	$('form').on('submit', function(e){
		e.preventDefault();

		let validate = true;

		$('form input+small').text('');

		$.each($('form input[required]'), function(index, value){
			if ( $(this).val() == '' ) {
				let key = $(this).attr('name');
				$(this).next().text(key + ' is required.');
				validate = false;
			} else {
				validate = true;
			}
		});

		if ( validate ) {
			$.ajax({
				type: 'POST',
				url: './server.php',
				cache: false,
	      contentType: false,
	      processData: false,
				data: new FormData(this),
				success: function(res) {
					let data = $.parseJSON(res);
					let html;
					html  = '<div>'+ data.name +'</div>';
					html += '<div>'+ data.phone +'</div>';
					html += '<div>'+ data.email +'</div>';
					html += '<div>'+ data.company +'</div>';
					html += '<div><img src="./uploads/'+ data.logo +'"></div>';
					$('#result').html(html);
					$('form input').val('');
					$('form input+small').text('');
				},
				error: function() {

				}
			});
		}


	});


});